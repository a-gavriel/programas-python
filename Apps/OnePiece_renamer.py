import os
import sys
import random
import shutil
import re


class Season:
  num : int
  start : int
  end : int
  name : str


seasons_data : list['Season'] = []

def read_seasons(folder : str) -> None:
  global seasons_data
  path : str = folder + "\\tvdb4.mapping"
  all_text_lines : list = []
  with open(path,"r") as f:
    all_text_lines = f.readlines()
  for line in all_text_lines:
    line = line.replace("\n","")
    if line.count("|") == 3:
      num_, start_, end_, name_ = line.split("|")
      temp = Season()
      temp.name = name_
      temp.num = int(num_)
      temp.start = int(start_)
      temp.end = int(end_)
      seasons_data.append(temp)

  return 



def iterating(rootdir):
    global seasons_data
    processsed_filenames = []
    for subdir, dirs, files in os.walk(rootdir):
        foldername = subdir[ len(rootdir) + 1: ]
        
        for filename in files:
          filename_nums = re.search(r'\d+', filename).group()
          extension = filename[ filename.index(".") + 1 :]
          
          if  (extension in ("mp4","mkv","avi")):
            if not (foldername[0].isdigit()):
              ep_num = int(filename_nums)
##              for season in seasons_data:
#                if season.start <= ep_num <= season.end:
                  #new_folder = f"{rootdir}\\Season {season.num:02} Arc {season.num:02} - {season.name}"
              new_folder = f"{rootdir}\\One Piece [{(ep_num//100)*100+1:04}-{(ep_num//100)*100+100:04}]"
              if not os.path.exists(new_folder):
                os.makedirs(new_folder)
              shutil.move(f"{subdir}\\{filename}",f"{new_folder}\\{filename}")
              processsed_filenames.append(filename)


          




def main():
  #read_seasons("D:\\Videos\\PLEX\\Plex Anime\\One Piece")
  iterating("D:\\Videos\\PLEX\\Plex Anime\\One Piece")

if __name__ == "__main__":
  main()