﻿create_canton(conn, 'Puntarenas', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Esparza', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Buenos Aires', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Montes de Oro', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Osa', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Quepos', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Golfito', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Coto Brus', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Parrita', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Corredores', 'Puntarenas', 'Costa Rica')
create_canton(conn, 'Garabito', 'Puntarenas', 'Costa Rica')

create_distrito(conn, 'Puntarenas','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Pitahaya','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Chomes','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Lepanto','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Paquera','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Manzanillo','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Guacimal','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Barranca','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Monte Verde','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Isla del Coco','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Cóbano','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Chacarita','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Chira','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Acapulco','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'El Roble','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Arancibia','Puntarenas', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Espíritu Santo','Esparza', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'San Juan Grande','Esparza', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Macacona','Esparza', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'San Rafael','Esparza', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'San Jerónimo','Esparza', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Caldera','Esparza', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Buenos Aires','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Volcán','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Potrero Grande','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Boruca','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Pilas','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Colinas','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Chánguena','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Biolley','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Brunka','Buenos Aires', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Miramar','Montes de Oro', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'La Unión','Montes de Oro', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'San Isidro','Montes de Oro', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Puerto Cortés','Osa', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Palmar','Osa', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Sierpe','Osa', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Bahía Ballena','Osa', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Piedras Blancas','Osa', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Bahía Drake','Osa', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Quepos','Quepos', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Savegre','Quepos', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Naranjito','Quepos', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Golfito','Golfito', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Puerto Jiménez','Golfito', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Guaycará','Golfito', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Pavón','Golfito', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'San Vito','Coto Brus', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Sabalito','Coto Brus', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Aguabuena','Coto Brus', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Limoncito','Coto Brus', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Pittier','Coto Brus', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Gutiérrez Braun','Coto Brus', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Parrita','Parrita', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Corredor','Corredores', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'La Cuesta','Corredores', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Canoas','Corredores', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Laurel','Corredores', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Jacó','Garabito', 'Puntarenas', 'Costa Rica')
create_distrito(conn, 'Tárcoles','Garabito', 'Puntarenas', 'Costa Rica')
