import requests
from bs4 import BeautifulSoup

# Define the URL to scrape
url = "https://crautos.com/autosusados/searchresults.cfm?p=1&c=10020"

# Send an HTTP GET request to the URL
response = requests.get(url)

# Check if the request was successful
if response.status_code == 200:
    # Parse the HTML content of the page
    soup = BeautifulSoup(response.text, 'html.parser')
    
    # Extract data from the HTML using BeautifulSoup
    # For example, let's extract the titles of all cars listed
    car_inventory = soup.find_all(None, class_="inventory")
    
    i = 0
    # Print the extracted car titles
    for inventory in car_inventory:
        title = inventory.find(None, class_="title")
        i += 1
        print(f'{i:2d}:', title.text.strip())
        
else:
    print("Failed to retrieve the page. Status code:", response.status_code)
