from PIL import Image, PngImagePlugin
from PIL import *
import sys
import time




def main():
  file_name : str = ""
  if len(sys.argv) > 1:
    file_name = sys.argv[1]
  else:
    file_name = input("Path to file:\n>>>")
  file_name = file_name.strip().replace('"','')
  if "\\" in file_name:
    output_name = file_name[file_name.rfind("\\")+1:file_name.rfind(".")]
  else:
    output_name = file_name[:file_name.rfind(".")]

  CANVAS_WIDTH = 2567
  CANVAS_HEIGHT = 3322
  CARDS_HORIZONTAL = 4
  CARDS_VERTICAL = 4
  CARDS_PER_PAGE = 16

  repeating_card  : PngImagePlugin.PngImageFile = Image.open(file_name)
  padding_horizontal = int((CANVAS_WIDTH - (repeating_card.width * CARDS_HORIZONTAL)) / ((CARDS_HORIZONTAL+1)))
  padding_vertical = int((CANVAS_HEIGHT - (repeating_card.height * CARDS_VERTICAL)) / ((CARDS_VERTICAL+1)))
  card_i = 0
  

  while (card_i < CARDS_PER_PAGE):
    if (card_i % CARDS_PER_PAGE) == 0:
      full_img = Image.new('RGB', (CANVAS_WIDTH, CANVAS_HEIGHT), color =(255,255,255) )

    out_card = repeating_card
    x = ((card_i//4))*repeating_card.width
    x += (padding_horizontal*((card_i//CARDS_HORIZONTAL)+1))
    y = (card_i%4)*repeating_card.height
    y += (padding_vertical*((card_i%CARDS_VERTICAL)+1))
    full_img.paste(out_card,(x,y))

    if ((card_i % CARDS_PER_PAGE) == (CARDS_PER_PAGE - 1)):
      full_img.save("set-"+ output_name + "-" + str(card_i// CARDS_PER_PAGE )+ ".png")

    card_i += 1

try:
   main()
except Exception as e:
   print("Error!\n", e)

for i in range(5,1,-1):
   print(f"Closing in {i}", end="\r")
   time.sleep(1)
      