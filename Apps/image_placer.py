from PIL import Image, ImageDraw, ImageFont
import random
import time

file_names = '''C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_09-58-42_.x000.y000.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x000.y000.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x000.y1378.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x1350.y1378.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x1800.y1378.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x2250.y1378.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x2700.y1378.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x3150.y1378.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x3600.y689.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x4050.y689.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x450.y1378.jpg
C:\\Users\\Alexis\\OneDrive\\Entretenimiento\\BoardGames\\Agricola\\zchop.2024-01-27_10-00-31_.x900.y1378.jpg
'''

files_list = file_names.splitlines()




def create_card(current_i):
  if current_i < len(files_list):
    current_filename = files_list[current_i].strip().replace('"','')
    newcard = Image.open(current_filename) 
  else:
    newcard = Image.new('RGB', (756, 760), color =(255,255,255) )
  return newcard


def main():
  cards_per_page = 12
  card_i = 0
  cards_padded = len(files_list) + ( cards_per_page - (len(files_list) % cards_per_page ))
  
  while (card_i < cards_padded):
    if (card_i % cards_per_page) == 0:
      full_img = Image.new('RGB', (2550, 3300), color =(150,150,150) )

    out_card = create_card(card_i)
    full_img.paste(out_card,(((card_i//4)%3)*800+50,(card_i%4)*800+50))

    if ((card_i % cards_per_page) == (cards_per_page - 1)):
      full_img.save("set"+ str(card_i// cards_per_page )+ ".png")

    card_i += 1

try:
   main()
except Exception as e:
   print("Error!\n", e)

for i in range(5,1,-1):
   print(f"Closing in {i}", end="\r")
   time.sleep(1)
      