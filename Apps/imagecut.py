from PIL import Image
import datetime
import sys
import time


def is_black(img : Image.Image)  -> bool:
   w = img.width
   h = img.height
   sample_w = w//20
   sample_h = h//20

   for x in range(sample_w, w-1, sample_w):
      for y in range(sample_h, h-1, sample_h):
         r,g,b = img.getpixel((x,y))
         if (r > 10) or (g > 10) or (b > 10):
            return False
   
   return True


def main():
   if len(sys.argv) > 1:
      infile = sys.argv[1]
   else:
      infile = input("Path to file:\n>>>")

   infile = infile.strip().replace('"','')

   if len(sys.argv) == 4:
      horizontal = int(sys.argv[2])
      vertical   = int(sys.argv[3])
   else:
      horizontal = int(input("horizontal imgs:"))
      vertical = int(input("vertical imgs:"))

   now = datetime.datetime.now()
   output_name =now.strftime("%Y-%m-%d_%H-%M-%S_")

   img = Image.open(infile)
   width, height = img.size

   chopsizeW, chopsizeH = width // horizontal, height // vertical

   # Save Chops of original image
   for x0 in range(0, width, chopsizeW):
      for y0 in range(0, height, chopsizeH):
         box = (x0, y0,
               x0+chopsizeW if x0+chopsizeW <  width else  width - 1,
               y0+chopsizeH if y0+chopsizeH < height else height - 1)
         
         temp_image = img.crop(box)
         print(box)
         if not is_black(temp_image):
            temp_image.save('zchop.%s.x%03d.y%03d.jpg' % (output_name, x0, y0))


try:
   main()
except Exception as e:
   print("Error!\n", e)

for i in range(5,1,-1):
   print(f"Closing in {i}", end="\r")
   time.sleep(1)
      
