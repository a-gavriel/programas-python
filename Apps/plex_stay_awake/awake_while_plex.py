import time
import pyautogui
import win32api
from random import randint
from datetime import datetime, timedelta
from file_read_backwards import FileReadBackwards
import os 
#importing the module 
import logging 

dir_path = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_path)

#now we will Create and configure logger 
logging.basicConfig(filename="plex_awake.log", 
					format='%(asctime)s %(message)s', 
					filemode='w') 

#Let us Create an object 
logger=logging.getLogger() 

#Now we are going to Set the threshold of logger to DEBUG 
logger.setLevel(logging.DEBUG) 

old_location : pyautogui.Point = pyautogui.Point(0,0)

PLEX_LOG_FILE = "C:\\Users\\Alexis\\AppData\\Local\\Plex Media Server\\Logs\\Plex Media Server.log"

def mouse_has_moved() -> bool:
  global old_location
  new_location = pyautogui.position()
  if new_location == old_location:
    return False
  else:
    old_location = new_location
    return True
  
def getIdleTime():
  return_val = (win32api.GetTickCount() - win32api.GetLastInputInfo()) / 1000.0
  #print(return_val)
  return return_val

def move_cursor() -> None:
  global old_location
  dx = randint(-2,3)
  dy = randint(-2,3)
  pyautogui.moveTo(old_location[0] + dx, old_location[1] + dy)
  new_location = pyautogui.position()
  logger.debug(f'>> Made movement from {old_location} to {new_location}')
  old_location = new_location
  return




def check_plex_usage():
  with FileReadBackwards(PLEX_LOG_FILE, encoding="utf-8") as BigFile:
    now = datetime.now()
    last_15 = now - timedelta(minutes=15)
    compare_time = False
    for full_line in BigFile:
      line = full_line
      try:
        if (" [" in line) and ("." in line) and (":" in line):
          line = line[:line.index(" [")] # Remove extra text
          line = line[:line.index(".")] # Remove millis
          time = datetime.strptime(line, '%b %d, %Y %H:%M:%S') # Get time of log
          compare_time = True
        else:
          compare_time = False
      except Exception as e:
        compare_time = False
        logger.debug(f">> Error: {e}")
        logger.debug(f">> Error parsing line:{line}")
      if compare_time:
        if (last_15 < time):
          if ("state playing" in full_line):
            return True
          else:
            continue
        else:
          return False
      else:
        continue
    return False
        



def main():
  global old_location
  CYCLES = 300
  TEN_S = 10
  PC_IDLE_TIME_S = 3000  # 50 mins
  old_location = pyautogui.position()
  pc_is_idle_lately = False
  has_client_lately = False

  while(True):
    # Wait for 10 mins before checking again
    for _ in range(0, CYCLES):
      time.sleep(TEN_S)
      has_client_lately = check_plex_usage()
      pc_is_idle_lately = getIdleTime() > PC_IDLE_TIME_S
      if pc_is_idle_lately:
        if has_client_lately:
          logger.debug(f'>> Plex client detected and PC is idle, triggering stay-awake')
          move_cursor()
          break
        else:
          logger.debug(f">> PC idle, no PLEX client, doing nothing")
      else:
        logger.debug(f'>> PC not idle, pausing stay-awake')
    if (not has_client_lately) and (pc_is_idle_lately):
      os.system('shutdown -s')


if __name__ == "__main__":
  main()